﻿using System;

namespace ReqLesson1Prog
{
    class Program
    {
        public static void Main(string[] args)
        {
            var arr = new int[] {21, 1, 5, 3, 7, 1, 18, 9, 0, 19};


            Console.WriteLine(Factorial(5));
        }

        public static long Pow(long number, int power)
        {
            if (power == 1)
                return number;
            if (power == 2)
                return number * number;
            if (power % 2 == 0)
            {
                var halfResult = Pow(number, power / 2);
                return halfResult * halfResult;
            }

            var almostHalfResult = Pow(number, power / 2);
            return almostHalfResult * almostHalfResult * number;
        }

        public static int Max(int[] array)
        {
            return Max(array, 0, array.Length);
        }

        public static int Max(int[] array, int from, int to)
        {
            var length = to - from;
            if (length == 1)
                return array[from];
            if (length == 2)
                return Math.Max(array[from], array[from + 1]);
            var middleIndex = from + length / 2;
            if (length % 2 == 0)
                return Math.Max(Max(array, from, middleIndex), Max(array, middleIndex, to));
            return Math.Max(array[to - 1], Math.Max(Max(array, from, middleIndex), Max(array, middleIndex, to)));
        }

        public static long AckermanFunction(long m, long n)
        {
            if (m == 0)
                return n + 1;
            if (n == 0)
                return AckermanFunction(m - 1, 1);
            return AckermanFunction(m - 1, AckermanFunction(m, n - 1));
        }

        public static int Factorial(int n)
        {
            var result = 1;
            Factorial(n, 1, ref result);
            return result;
        }

        public static void Factorial(int n, int currentN, ref int result)
        {
            result = result * currentN;
            if (currentN == n)
                return;
            Factorial(n, currentN + 1, ref result);
        }
    }
}
